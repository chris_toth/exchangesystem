﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace BusinessLogic
{
    public class BL
    {
        #region -- fields and properties --
        // Daily exchange rates stored in memory
        private List<ExchangeNode> todaysExchangeRates = new List<ExchangeNode>();
        public List<ExchangeNode> DailyExchangeRates
        {
            get
            {
                return todaysExchangeRates;
            }

            private set
            {
                todaysExchangeRates = value;
            }
        }
        #endregion

        #region -- main methods --
        public void RefreshExchangeRatesForToday()
        {
            // This method downloads the daily rates from the provided URL and
            // saves is in an in-memory object.
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(
                "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
                );
            XDocument xDoc = ToXDocument(xmlDoc);
            // Selecting the container cube of each day and its elements
            List<XElement> cubes = xDoc.Descendants()
                   .Attributes("time")
                   .Select(x => x.Parent)
                   .ToList();
            // The first cube is filled with the current rates at the 0 index
            List<XElement> today = cubes[0].Elements().ToList();
            // Redefine list
            todaysExchangeRates = new List<ExchangeNode>();
            todaysExchangeRates.Add(new ExchangeNode() {
                Currency = CurrencyType.EUR,
                Rate = 1
            });
            for (int i = 0; i < today.Count; i++)
            {
                // Add the fresh nodes
                todaysExchangeRates.Add(new ExchangeNode()
                { 
                    Currency = (CurrencyType)Enum.Parse(typeof(CurrencyType),today[i].Attribute("currency").Value.ToString()),
                    Rate = Double.Parse(today[i].Attribute("rate").Value)
                });
            }
            GenerateDataNodesForRickshaw();
        }
        public double ExchangeCurrency(ExchangeNode exchangeFrom, ExchangeNode exchangeTo, int ammount)
        {
            // This method is used for the calculation part of the exchange mechanism
            // provided 2 currency types and the ammount to be exchanged.
            return ammount * exchangeFrom.Rate / exchangeTo.Rate;
        }
        #endregion

        #region -- Rickshaw generation --

        public List<RickshawDataNode> GenerateDataNodesForRickshaw()
        {
            // This method returns the series of data that will later be drawn
            // on the rickshaw - graph. It's viewable on the "Historical timeline"
            // page.
            List<RickshawDataNode> dataNodes = new List<RickshawDataNode>();            
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(
                "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
                );
            XDocument xDoc = ToXDocument(xmlDoc);
            // Selecting the container cube of each day and its elements
            List<XElement> cubes = xDoc.Descendants()
                   .Attributes("time")
                   .Select(x => x.Parent)
                   .ToList();
            // Selecting the last timestamp, because that belongs to the first array
            // of data in chronological order.
            var timeStamp = xDoc.Descendants()
                .Attributes("time")
                .Select(x => x.Value)
                .ToList().Last();
            
            // adding main data for the nodes -> currency name and color
            for (int j = 0; j < 31; j++)
            {
                dataNodes.Add(new RickshawDataNode()
                {
                    Name = ((CurrencyType)j).ToString(),
                    Color = "green",
                    Data = new List<string>()
                });
            }
            DateTimeOffset startDate = DateTimeOffset.Parse(timeStamp);
            long x_axis = startDate.ToUnixTimeSeconds(); // to give each day and x-axis representation
            foreach (XElement cube in cubes)
            {
                // selecting each day without the container, currencies only
                List<XElement> dailyCubes = cube.Descendants()
                    .Attributes("currency")
                    .Select(x => x.Parent).ToList();

                // going through the daily rates to add data
                for (int i = 0; i < dailyCubes.Count; i++)
                {
                    dataNodes[i].Data.Add("{ x: "+x_axis+", y: "+ dailyCubes[i].Attribute("rate").Value + ",},");
                }
                x_axis+= 86400;                           
            }
            return dataNodes;       
        }

        #endregion

        #region -- xml document conversions --
        public XmlDocument ToXmlDocument(XDocument xDocument)
        {
            // Method to convert a given XDocument format into an XmlDocument
            // format.
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }
        public XDocument ToXDocument(XmlDocument xmlDocument)
        {
            // Method to convert a given XmlDocument format into an XDocument
            // format.
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }
        #endregion
    }
}
