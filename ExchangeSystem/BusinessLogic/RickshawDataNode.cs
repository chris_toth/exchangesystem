﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    // Class representation for each plot-line in the rickshaw graph
    // that is generated.
    // Each currency has: a color, a name and the values for each day.
    public class RickshawDataNode
    {
        public string Color { get; set; }
        public string Name { get; set; }
        public List<string> Data { get; set; }
    }
}
