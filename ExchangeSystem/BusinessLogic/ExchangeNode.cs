﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    // Class to abstract each country's currency
    public class ExchangeNode
    {
        // HUF / JEN / etc.
        public CurrencyType Currency { get; set; }
        // 1.12321 , 27.4534 , etc.
        public double Rate { get; set; }
    }
}
