﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class ExchangeRequest
    {
        public CurrencyType exchangeFrom { get; set; }
        public CurrencyType exchangeTo { get; set; }
        public int ammountToExchange { get; set; }
    }

    public enum CurrencyType {
        USD,
        JPY,
        BGN,
        CZK,
        DKK,
        GBP,
        HUF,
        PLN,
        RON,
        SEK,
        CHF,
        NOK,
        HRK,
        RUB,
        TRY,
        AUD,
        BRL,
        CAD,
        CNY,
        HKD,
        IDR,
        ILS,
        INR,
        KRW,
        MXN,
        MYR,
        NZD,
        PHP,
        SGD,
        THB,
        ZAR,
        EUR
    }
}
