﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExchangeSystem.Controllers
{
    public class HomeController : Controller
    {
        private BusinessLogic.BL businessLogic = new BusinessLogic.BL();
        [HttpGet]
        public ActionResult Index()
        {
            businessLogic.RefreshExchangeRatesForToday();
            return View();
        }
        [HttpPost]
        public string Index(BusinessLogic.ExchangeRequest exModel)
        {
            businessLogic.RefreshExchangeRatesForToday();
            int j = 0; int k = 0;
            for (int i = 0; i < businessLogic.DailyExchangeRates.Count; i++)
            {
                if (exModel.exchangeFrom == businessLogic.DailyExchangeRates[i].Currency) { j = i; }
                if (exModel.exchangeTo == businessLogic.DailyExchangeRates[i].Currency) { k = i; }
            }
            double exchangedAmmount = (exModel.ammountToExchange * (businessLogic.DailyExchangeRates[k].Rate / businessLogic.DailyExchangeRates[j].Rate));
            return "The total ammount you should recieve is: [" + exchangedAmmount.ToString("F2")
                .ToString() + "] "
                + exModel.exchangeTo;
        }
        public ActionResult GetExchangeRates()
        {
            businessLogic.RefreshExchangeRatesForToday();
            return PartialView("_TodaysRates", businessLogic.DailyExchangeRates);
        }

        public ActionResult TimeLine()
        {
            return View(businessLogic.GenerateDataNodesForRickshaw());
        }
    }
}