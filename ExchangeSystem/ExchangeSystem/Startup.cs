﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExchangeSystem.Startup))]
namespace ExchangeSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
